// tslint:disable 

/**
 * Container object for steps in a trade sequence
 */
export interface Recipe {
  steps: {
    price?: number;
    amount?: number;
    pairName: string;
    pairIndex: number;
    /**
     * What kind of order
     */
    orderType?: "market" | "limit";
    /**
     * Which way a trade goes
     */
    direction?: "buy" | "sell";
    orderId?: string;
  }[];
  initialAssetIndex: number;
  initialAssetName: string;
  initialAmount: number;
  guardList: string[];
}


/**
 * Step in a recipe
 */
export interface Step {
  price?: number;
  amount?: number;
  pairName: string;
  pairIndex: number;
  /**
   * What kind of order
   */
  orderType?: "market" | "limit";
  /**
   * Which way a trade goes
   */
  direction?: "buy" | "sell";
  orderId?: string;
}


/**
 * Trader state object
 */
export interface TraderState {
  stillProcessing: boolean;
}


