// tslint:disable 

/**
 * Request. Add new order.
 */
export interface AddOrder {
  event: "addOrder";
  /**
   * Session token string
   */
  token: string;
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  ordertype:
    | "market"
    | "limit"
    | "stop-loss"
    | "take-profit"
    | "stop-loss-profit"
    | "stop-loss-profit-limit"
    | "stop-loss-limit"
    | "take-profit-limit"
    | "trailing-stop"
    | "trailing-stop-limit"
    | "stop-loss-and-limit"
    | "settle-position";
  /**
   * type of order (buy/sell)
   */
  type: "buy" | "sell";
  pair: string;
  /**
   * Optional dependent on order type - order price
   */
  price?: number;
  /**
   * Optional dependent on order type - order secondary price
   */
  price2?: number;
  /**
   * Order volume in lots
   */
  volume: string;
  /**
   * amount of leverage desired (optional; default = none)
   */
  leverage?: number;
  /**
   * Optional - comma delimited list of order flags. viqc = volume in quote currency (not currently available), fcib = prefer fee in base currency, fciq = prefer fee in quote currency, nompp = no market price protection, post = post only order (available when ordertype = limit)
   */
  oflags?: string;
  /**
   * Optional - scheduled start time. 0 = now (default) +<n> = schedule start time <n> seconds from now <n> = unix timestamp of start time
   */
  starttm?: number;
  /**
   * Optional - expiration time. 0 = no expiration (default) +<n> = expire <n> seconds from now <n> = unix timestamp of expiration time
   */
  expiretm?: number;
  /**
   * Optional - user reference ID (should be an integer in quotes)
   */
  userref?: string;
  /**
   * Optional - validate inputs only; do not submit order (not currently available)
   */
  validate?: string;
  /**
   * Optional - close order type.
   */
  "close[ordertype]"?: string;
  /**
   * Optional - close order price.
   */
  "close[price]"?: number;
  /**
   * Optional - close order secondary price.
   */
  "close[price2]"?: number;
  /**
   * Optional - should be set to 'agree' by German residents in order to signify acceptance of the terms of the Kraken Trading Agreement.
   */
  trading_agreement?: string;
}


/**
 * Response. Add new order.
 */
export type AddOrderStatus = {
  event: "addOrderStatus" | "error";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  /**
   * Status. 'ok' or 'error'
   */
  status: "ok" | "error";
  [k: string]: unknown;
} & (
  | {
      /**
       * order ID (if successful)
       */
      txid: string;
      /**
       * order description info (if successful)
       */
      descr: string;
    }
  | {
      pair: string;
      /**
       * error message (if unsuccessful)
       */
      errorMessage: string;
    }
);


export interface AssetPair {
  /**
   * alternate pair name
   */
  altname?: string;
  /**
   * WebSocket pair name (if available)
   */
  wsname: string;
  /**
   * asset class of base component
   */
  aclass_base?: string;
  /**
   * asset id of base component
   */
  base: string;
  /**
   * asset class of quote component
   */
  aclass_quote?: string;
  /**
   * asset id of quote component
   */
  quote: string;
  /**
   * volume lot size
   */
  lot?: string;
  /**
   * scaling decimal places for pair
   */
  pair_decimals: number;
  /**
   * scaling decimal places for volume
   */
  lot_decimals?: number;
  /**
   * amount to multiply lot volume by to get currency volume
   */
  lot_multiplier?: number;
  /**
   * array of leverage amounts available when buying
   */
  leverage_buy?: number[];
  /**
   * array of leverage amounts available when selling
   */
  leverage_sell?: number[];
  /**
   * fee schedule array in [volume, percent fee] tuples
   */
  fees: [number, number][];
  /**
   * fee schedule array in [volume, percent fee] tuples (if on maker/taker)
   */
  fees_maker: [number, number][];
  /**
   * volume discount currency
   */
  fee_volume_currency?: string;
  /**
   * margin call level
   */
  margin_call?: number;
  /**
   * stop-out/liquidation margin level
   */
  margin_stop?: number;
  /**
   * minimum order volume for pair
   */
  ordermin?: string;
}


/**
 * Publication: Order book levels. On subscription, a snapshot will be published at the specified depth, following the snapshot, level updates will be published
 */
export interface BookSnapshot {
  /**
   * Array of price levels, ascending from best ask
   */
  as: [number, number, number][];
  /**
   * Array of price levels, descending from best bid
   */
  bs: [number, number, number][];
}


/**
 * Publication: Order book levels. On subscription, a snapshot will be published at the specified depth, following the snapshot, level updates will be published
 */
export type BookUpdate =
  | {
      /**
       * Ask array of level updates
       */
      a: [number, number, number] | [number, number, number, string][];
      /**
       * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
       */
      c: string;
    }
  | {
      /**
       * Bid array of level updates
       */
      b: [number, number, number] | [number, number, number, string][];
      /**
       * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
       */
      c: string;
    };


/**
 * Request. Cancel order or list of orders.
 */
export interface CancelOrder {
  event: "cancelOrder";
  /**
   * Session token string
   */
  token: string;
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  /**
   * Array of order IDs to be canceled. These can be user reference IDs.
   */
  txid: string[];
}


/**
 * Response. Cancel order or list of orders.
 */
export interface CancelOrderStatus {
  event: "cancelOrderStatus" | "error";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  /**
   * Status. 'ok' or 'error'
   */
  status: "ok" | "error";
  /**
   * error message (if unsuccessful)
   */
  errorMessage?: string;
}


/**
 * type of order (buy/sell)
 */
export type Direction = "buy" | "sell";


export type EventRequest = "subscribe" | "unsubscribe" | "ping" | "addOrder" | "cancelOrder";


export type EventResponse =
  | "pong"
  | "heartbeat"
  | "systemStatus"
  | "subscriptionStatus"
  | "addOrderStatus"
  | "cancelOrderStatus"
  | "error";


/**
 * Publication: Server heartbeat sent if no subscription traffic within 1 second (approximately)
 */
export interface Heartbeat {
  event: "heartbeat";
}


/**
 * Publication: Open High Low Close (Candle) feed for a currency pair and interval period.
 */
export type OpenHighLowClose = [number, number, number, number, number, number, number, number, number];


/**
 * Publication: Open orders. Feed to show all the open orders belonging to the user authenticated API key. Initial snapshot will provide list of all open orders and then any updates to the open orders list will be sent. For status change updates, such as 'closed', the fields orderid and status will be present in the payload.
 */
export type OpenOrders = {
  /**
   * Order object
   */
  [k: string]: {
    /**
     * Referral order transaction id that created this order
     */
    refid: string;
    /**
     * user reference ID
     */
    userref: number;
    /**
     * status of order
     */
    status: string;
    /**
     * unix timestamp of when order was placed
     */
    opentm: number;
    /**
     * unix timestamp of order start time (if set)
     */
    starttm?: number;
    /**
     * unix timestamp of order end time (if set)
     */
    expiretm?: number;
    descr: {
      pair: string;
      /**
       * Optional - position ID (if applicable)
       */
      position?: string;
      /**
       * type of order (buy/sell)
       */
      type: "buy" | "sell";
      ordertype:
        | "market"
        | "limit"
        | "stop-loss"
        | "take-profit"
        | "stop-loss-profit"
        | "stop-loss-profit-limit"
        | "stop-loss-limit"
        | "take-profit-limit"
        | "trailing-stop"
        | "trailing-stop-limit"
        | "stop-loss-and-limit"
        | "settle-position";
      /**
       * primary price
       */
      price: number;
      /**
       * secondary price
       */
      price2: number;
      /**
       * amount of leverage
       */
      leverage: number;
      /**
       * order description
       */
      order: string;
      /**
       * conditional close order description (if conditional close set)
       */
      close: string;
    };
    /**
     * volume of order (base currency unless viqc set in oflags)
     */
    vol: number;
    /**
     * total volume executed so far (base currency unless viqc set in oflags)
     */
    vol_exec: number;
    /**
     * total cost (quote currency unless unless viqc set in oflags)
     */
    cost: number;
    /**
     * total fee (quote currency)
     */
    fee: number;
    /**
     * average price (cumulative; quote currency unless viqc set in oflags)
     */
    avg_price: number;
    /**
     * stop price (quote currency, for trailing stops)
     */
    stopprice: number;
    /**
     * triggered limit price (quote currency, when limit based order type triggered)
     */
    limitprice: number;
    /**
     * comma delimited list of miscellaneous info: stopped=triggered by stop price, touched=triggered by touch price, liquidation=liquidation, partial=partial fill
     */
    misc: string;
    /**
     * Optional - comma delimited list of order flags. viqc = volume in quote currency (not currently available), fcib = prefer fee in base currency, fciq = prefer fee in quote currency, nompp = no market price protection, post = post only order (available when ordertype = limit)
     */
    oflags?: string;
    /**
     * Optional - cancel reason, present for all cancellation updates (status='canceled') and for some close updates (status='closed')
     */
    cancel_reason?: string;
  };
}[];


/**
 * list of order flags. viqc = volume in quote currency (not currently available), fcib = prefer fee in base currency, fciq = prefer fee in quote currency, nompp = no market price protection, post = post only order (available when ordertype = limit)
 */
export type OrderFlags = "vqic" | "fcib" | "fciq" | "nompp" | "post";


export type OrderType =
  | "market"
  | "limit"
  | "stop-loss"
  | "take-profit"
  | "stop-loss-profit"
  | "stop-loss-profit-limit"
  | "stop-loss-limit"
  | "take-profit-limit"
  | "trailing-stop"
  | "trailing-stop-limit"
  | "stop-loss-and-limit"
  | "settle-position";


/**
 * Publication: Own trades. On subscription last 50 trades for the user will be sent, followed by new trades.
 */
export type OwnTrades = {
  /**
   * Trade object
   */
  [k: string]: {
    /**
     * order responsible for execution of trade
     */
    ordertxid: string;
    /**
     * Position trade id
     */
    postxid: string;
    pair: string;
    /**
     * unix timestamp of trade
     */
    time: number;
    /**
     * type of order (buy/sell)
     */
    type: "buy" | "sell";
    orderType:
      | "market"
      | "limit"
      | "stop-loss"
      | "take-profit"
      | "stop-loss-profit"
      | "stop-loss-profit-limit"
      | "stop-loss-limit"
      | "take-profit-limit"
      | "trailing-stop"
      | "trailing-stop-limit"
      | "stop-loss-and-limit"
      | "settle-position";
    /**
     * average price order was executed at (quote currency)
     */
    price: number;
    /**
     * total cost of order (quote currency)
     */
    cost: number;
    /**
     * total fee (quote currency)
     */
    fee: number;
    /**
     * volume (base currency)
     */
    vol: number;
    /**
     * initial margin (quote currency)
     */
    margin: number;
  };
}[];


/**
 * Request. Client can ping server to determine whether connection is alive, server responds with pong. This is an application level ping as opposed to default ping in websockets standard which is server initiated
 */
export interface Ping {
  event: "ping";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
}


/**
 * Response. Server pong response to a ping to determine whether connection is alive. This is an application level pong as opposed to default pong in websockets standard which is sent by client in response to a ping
 */
export interface Pong {
  event: "pong";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
}


/**
 * Container for private publications
 */
export type PrivatePublication = [
  (
    | {
        /**
         * Trade object
         */
        [k: string]: {
          /**
           * order responsible for execution of trade
           */
          ordertxid: string;
          /**
           * Position trade id
           */
          postxid: string;
          pair: string;
          /**
           * unix timestamp of trade
           */
          time: number;
          /**
           * type of order (buy/sell)
           */
          type: "buy" | "sell";
          orderType:
            | "market"
            | "limit"
            | "stop-loss"
            | "take-profit"
            | "stop-loss-profit"
            | "stop-loss-profit-limit"
            | "stop-loss-limit"
            | "take-profit-limit"
            | "trailing-stop"
            | "trailing-stop-limit"
            | "stop-loss-and-limit"
            | "settle-position";
          /**
           * average price order was executed at (quote currency)
           */
          price: number;
          /**
           * total cost of order (quote currency)
           */
          cost: number;
          /**
           * total fee (quote currency)
           */
          fee: number;
          /**
           * volume (base currency)
           */
          vol: number;
          /**
           * initial margin (quote currency)
           */
          margin: number;
        };
      }[]
    | {
        /**
         * Order object
         */
        [k: string]: {
          /**
           * Referral order transaction id that created this order
           */
          refid: string;
          /**
           * user reference ID
           */
          userref: number;
          /**
           * status of order
           */
          status: string;
          /**
           * unix timestamp of when order was placed
           */
          opentm: number;
          /**
           * unix timestamp of order start time (if set)
           */
          starttm?: number;
          /**
           * unix timestamp of order end time (if set)
           */
          expiretm?: number;
          descr: {
            pair: string;
            /**
             * Optional - position ID (if applicable)
             */
            position?: string;
            /**
             * type of order (buy/sell)
             */
            type: "buy" | "sell";
            ordertype:
              | "market"
              | "limit"
              | "stop-loss"
              | "take-profit"
              | "stop-loss-profit"
              | "stop-loss-profit-limit"
              | "stop-loss-limit"
              | "take-profit-limit"
              | "trailing-stop"
              | "trailing-stop-limit"
              | "stop-loss-and-limit"
              | "settle-position";
            /**
             * primary price
             */
            price: number;
            /**
             * secondary price
             */
            price2: number;
            /**
             * amount of leverage
             */
            leverage: number;
            /**
             * order description
             */
            order: string;
            /**
             * conditional close order description (if conditional close set)
             */
            close: string;
          };
          /**
           * volume of order (base currency unless viqc set in oflags)
           */
          vol: number;
          /**
           * total volume executed so far (base currency unless viqc set in oflags)
           */
          vol_exec: number;
          /**
           * total cost (quote currency unless unless viqc set in oflags)
           */
          cost: number;
          /**
           * total fee (quote currency)
           */
          fee: number;
          /**
           * average price (cumulative; quote currency unless viqc set in oflags)
           */
          avg_price: number;
          /**
           * stop price (quote currency, for trailing stops)
           */
          stopprice: number;
          /**
           * triggered limit price (quote currency, when limit based order type triggered)
           */
          limitprice: number;
          /**
           * comma delimited list of miscellaneous info: stopped=triggered by stop price, touched=triggered by touch price, liquidation=liquidation, partial=partial fill
           */
          misc: string;
          /**
           * Optional - comma delimited list of order flags. viqc = volume in quote currency (not currently available), fcib = prefer fee in base currency, fciq = prefer fee in quote currency, nompp = no market price protection, post = post only order (available when ordertype = limit)
           */
          oflags?: string;
          /**
           * Optional - cancel reason, present for all cancellation updates (status='canceled') and for some close updates (status='closed')
           */
          cancel_reason?: string;
        };
      }[]
  ),
  number
];


/**
 * Container for public publications
 */
export type Publication = [
  number,
  (
    | {
        /**
         * Ask
         */
        a: [number, number, number];
        /**
         * Bid
         */
        b: [number, number, number];
        /**
         * Close
         */
        c: [number, number];
        /**
         * Volume
         */
        v: [number, number];
        /**
         * Volume weighted average price
         */
        p: [number, number];
        /**
         * Number of trades
         */
        t: [number, number];
        /**
         * Low price
         */
        l: [number, number];
        /**
         * High price
         */
        h: [number, number];
        /**
         * Open price
         */
        o: [number, number];
      }
    | [number, number, number, number, number, number, number, number, number]
    | [number, number, number, string, string, string][]
    | [number, number, number, number, number]
    | {
        /**
         * Array of price levels, ascending from best ask
         */
        as: [number, number, number][];
        /**
         * Array of price levels, descending from best bid
         */
        bs: [number, number, number][];
      }
    | (
        | {
            /**
             * Ask array of level updates
             */
            a: [number, number, number] | [number, number, number, string][];
            /**
             * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
             */
            c: string;
          }
        | {
            /**
             * Bid array of level updates
             */
            b: [number, number, number] | [number, number, number, string][];
            /**
             * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
             */
            c: string;
          }
      )
  ),
  number,
  string
];


/**
 * Container for public publications
 */
export type PublicationPayload =
  | {
      /**
       * Ask
       */
      a: [number, number, number];
      /**
       * Bid
       */
      b: [number, number, number];
      /**
       * Close
       */
      c: [number, number];
      /**
       * Volume
       */
      v: [number, number];
      /**
       * Volume weighted average price
       */
      p: [number, number];
      /**
       * Number of trades
       */
      t: [number, number];
      /**
       * Low price
       */
      l: [number, number];
      /**
       * High price
       */
      h: [number, number];
      /**
       * Open price
       */
      o: [number, number];
    }
  | [number, number, number, number, number, number, number, number, number]
  | [number, number, number, string, string, string][]
  | [number, number, number, number, number]
  | {
      /**
       * Array of price levels, ascending from best ask
       */
      as: [number, number, number][];
      /**
       * Array of price levels, descending from best bid
       */
      bs: [number, number, number][];
    }
  | (
      | {
          /**
           * Ask array of level updates
           */
          a: [number, number, number] | [number, number, number, string][];
          /**
           * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
           */
          c: string;
        }
      | {
          /**
           * Bid array of level updates
           */
          b: [number, number, number] | [number, number, number, string][];
          /**
           * Optional - Book checksum as a quoted unsigned 32-bit integer, present only within the last update container in the message.
           */
          c: string;
        }
    );


export type Spread = [number, number, number, number, number];


/**
 * Status of subscription
 */
export type StatusOfSubscription = "subscribed" | "unsubscribed" | "error";


/**
 * Request. Subscribe to a topic on a single or multiple currency pairs.
 */
export interface Subscribe {
  event: "subscribe";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  pair?: string[];
  subscription: {
    /**
     * Optional - depth associated with book subscription in float of levels each side, default 10. Valid Options are: 10, 25, 100, 500, 1000
     */
    depth?: 10 | 25 | 100 | 500 | 1000;
    /**
     * Optional - Time interval associated with ohlc subscription in minutes. Default 1. Valid Interval values: 1|5|15|30|60|240|1440|10080|21600
     */
    interval?: 1 | 5 | 15 | 30 | 60 | 240 | 1440 | 10080 | 21600;
    /**
     * book|ohlc|openOrders|ownTrades|spread|ticker|trade|*, * for all available channels depending on the connected environment
     */
    name?: "book" | "ohlc" | "openOrders" | "ownTrades" | "spread" | "ticker" | "trade" | "*";
    /**
     * Optional - whether to send historical feed data snapshot upon subscription (supported only for ownTrades subscriptions; default = true)
     */
    snapshot?: boolean;
    /**
     * Optional - base64-encoded authentication token for private-data endpoints
     */
    token?: string;
  };
}


export interface Subscription {
  /**
   * Optional - depth associated with book subscription in float of levels each side, default 10. Valid Options are: 10, 25, 100, 500, 1000
   */
  depth?: 10 | 25 | 100 | 500 | 1000;
  /**
   * Optional - Time interval associated with ohlc subscription in minutes. Default 1. Valid Interval values: 1|5|15|30|60|240|1440|10080|21600
   */
  interval?: 1 | 5 | 15 | 30 | 60 | 240 | 1440 | 10080 | 21600;
  /**
   * book|ohlc|openOrders|ownTrades|spread|ticker|trade|*, * for all available channels depending on the connected environment
   */
  name?: "book" | "ohlc" | "openOrders" | "ownTrades" | "spread" | "ticker" | "trade" | "*";
  /**
   * Optional - whether to send historical feed data snapshot upon subscription (supported only for ownTrades subscriptions; default = true)
   */
  snapshot?: boolean;
  /**
   * Optional - base64-encoded authentication token for private-data endpoints
   */
  token?: string;
}


/**
 * book|ohlc|openOrders|ownTrades|spread|ticker|trade|*, * for all available channels depending on the connected environment
 */
export type SubscriptionName = "book" | "ohlc" | "openOrders" | "ownTrades" | "spread" | "ticker" | "trade" | "*";


/**
 * Response. Subscription status response to subscribe, unsubscribe or exchange initiated unsubscribe.
 */
export type SubscriptionStatus = {
  event: "subscriptionStatus" | "error";
  /**
   * Optional - client originated ID reflected in response message
   */
  reqid?: number;
  pair: string;
  /**
   * Status of subscription
   */
  status: "subscribed" | "unsubscribed" | "error";
  subscription: {
    /**
     * Optional - depth associated with book subscription in float of levels each side, default 10. Valid Options are: 10, 25, 100, 500, 1000
     */
    depth?: 10 | 25 | 100 | 500 | 1000;
    /**
     * Optional - Time interval associated with ohlc subscription in minutes. Default 1. Valid Interval values: 1|5|15|30|60|240|1440|10080|21600
     */
    interval?: 1 | 5 | 15 | 30 | 60 | 240 | 1440 | 10080 | 21600;
    /**
     * book|ohlc|openOrders|ownTrades|spread|ticker|trade|*, * for all available channels depending on the connected environment
     */
    name?: "book" | "ohlc" | "openOrders" | "ownTrades" | "spread" | "ticker" | "trade" | "*";
    /**
     * Optional - whether to send historical feed data snapshot upon subscription (supported only for ownTrades subscriptions; default = true)
     */
    snapshot?: boolean;
    /**
     * Optional - base64-encoded authentication token for private-data endpoints
     */
    token?: string;
  };
} & (
  | {
      /**
       * Error message
       */
      errorMessage: string;
    }
  | {
      /**
       * ChannelID associated with pair subscription
       */
      channelID: number;
      /**
       * Channel Name on successful subscription. For payloads 'ohlc' and 'book', respective interval or depth will be added as suffix.
       */
      channelName: number;
    }
);


/**
 * Publication: Status sent on connection or system status changes.
 */
export interface SystemStatus {
  event: "systemStatus";
  connectionId: string;
  status: "online" | "maintenance";
  version: string;
}


/**
 * Publication: Ticker information on currency pair.
 */
export interface Ticker {
  /**
   * Ask
   */
  a: [number, number, number];
  /**
   * Bid
   */
  b: [number, number, number];
  /**
   * Close
   */
  c: [number, number];
  /**
   * Volume
   */
  v: [number, number];
  /**
   * Volume weighted average price
   */
  p: [number, number];
  /**
   * Number of trades
   */
  t: [number, number];
  /**
   * Low price
   */
  l: [number, number];
  /**
   * High price
   */
  h: [number, number];
  /**
   * Open price
   */
  o: [number, number];
}


export interface Token {
  token: string;
  expires: number;
}


export type Trade = [number, number, number, string, string, string][];


/**
 * Request. Unsubscribe, can specify a channelID or multiple currency pairs.
 */
export type Unsubscribe =
  | {
      event: "unsubscribe";
      /**
       * Optional - client originated ID reflected in response message
       */
      reqid?: number;
      pair?: string[];
      subscription: {
        /**
         * Optional - depth associated with book subscription in float of levels each side, default 10. Valid Options are: 10, 25, 100, 500, 1000
         */
        depth?: 10 | 25 | 100 | 500 | 1000;
        /**
         * Optional - Time interval associated with ohlc subscription in minutes. Default 1. Valid Interval values: 1|5|15|30|60|240|1440|10080|21600
         */
        interval?: 1 | 5 | 15 | 30 | 60 | 240 | 1440 | 10080 | 21600;
        /**
         * book|ohlc|openOrders|ownTrades|spread|ticker|trade|*, * for all available channels depending on the connected environment
         */
        name?: "book" | "ohlc" | "openOrders" | "ownTrades" | "spread" | "ticker" | "trade" | "*";
        /**
         * Optional - whether to send historical feed data snapshot upon subscription (supported only for ownTrades subscriptions; default = true)
         */
        snapshot?: boolean;
        /**
         * Optional - base64-encoded authentication token for private-data endpoints
         */
        token?: string;
      };
    }
  | {
      event: "subscribe" | "unsubscribe" | "ping" | "addOrder" | "cancelOrder";
      /**
       * Optional - client originated ID reflected in response message
       */
      reqid?: number;
      /**
       * ChannelID associated with pair subscription
       */
      channelID: number;
    };


/**
 * ChannelID associated with pair subscription
 */
export type ChannelID = number;


/**
 * Channel Name on successful subscription. For payloads 'ohlc' and 'book', respective interval or depth will be added as suffix.
 */
export type ChannelName = number;


/**
 * Optional - Array of currency pairs. Format of each pair is 'A/B', where A and B are ISO 4217-A3 for standardized assets and popular unique symbol if not standardized.
 */
export type Pair = string;


/**
 * Optional - client originated ID reflected in response message
 */
export type Reqid = number;


