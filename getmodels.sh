#!/bin/bash -ex
kraken_version=$1
trade_version=$2
mkdir -p temp
cd temp
wget http://gitlab.com/krispyensign/kraken-schema/-/archive/v$kraken_version/kraken-schema-v$kraken_version.zip
wget https://gitlab.com/krispyensign/generic-trade-schema/-/archive/v$trade_version/generic-trade-schema-v$trade_version.zip
unzip -o kraken-schema-v$kraken_version.zip
unzip -o generic-trade-schema-v$trade_version.zip
rm -fr *.zip
rm -fr *.zip.*
rm -fr kraken/
rm -fr trader/
rm -fr exchange/
mv -f kraken-schema-v$kraken_version/kraken kraken/
rm -fr kraken-schema-v$kraken_version/
mv -f generic-trade-schema-v$trade_version/trader/ trader/
mv -f generic-trade-schema-v$trade_version/exchange/ exchange/
rm -fr generic-trade-schema-v$trade_version/
node ../generateModels.mjs
cd ../
cp temp/*.d.ts .
rm -fr temp/