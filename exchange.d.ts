// tslint:disable 

/**
 * Which way a trade goes
 */
export type Direction = "buy" | "sell";


/**
 * The various schema names
 */
export type ExchangeName = "kraken" | "binance" | "cexio" | "coinbase";


/**
 * Fundamental Exchange Pair
 */
export interface ExchangePair {
  index: number;
  name: string;
  tradename: string;
  decimals: number;
  baseName: string;
  quoteName: string;
  makerFee: number;
  takerFee: number;
  volume: number;
  ordermin: number;
  ask?: number;
  bid?: number;
}


/**
 * ExchangePair that supports indices for computations
 */
export type IndexedPair = {
  index: number;
  name: string;
  tradename: string;
  decimals: number;
  baseName: string;
  quoteName: string;
  makerFee: number;
  takerFee: number;
  volume: number;
  ordermin: number;
  ask?: number;
  bid?: number;
} & {
  quoteIndex: number;
  baseIndex: number;
};


/**
 * What kind of order
 */
export interface OrderCancelRequest {
  event: "cancel";
  orderId?: string;
}


/**
 * What kind of order
 */
export interface OrderCancelResponse {
  event?: "cancel";
  order?: string[];
  result?: "ok" | "error";
}


/**
 * Order Create Request
 */
export interface OrderCreateRequest {
  event: "create";
  orderId: string;
  pair: string;
  /**
   * Which way a trade goes
   */
  direction: "buy" | "sell";
  /**
   * What kind of order
   */
  orderType: "market" | "limit";
  amount: number;
  price?: number;
}


/**
 * Order Create Request
 */
export interface OrderCreateResponse {
  event?: "create";
  orderId?: string;
  transactionId?: string;
  result?: "ok" | "error";
}


/**
 * What kind of order
 */
export type OrderEvent = "create" | "cancel" | "modify";


/**
 * What kind of order
 */
export interface OrderModifyRequest {
  event: "modify";
  orderId: string;
  pair: string;
  /**
   * Which way a trade goes
   */
  direction: "buy" | "sell";
  /**
   * What kind of order
   */
  orderType: "market" | "limit";
  amount: number;
  price?: number;
  orders: string[];
}


/**
 * What kind of order
 */
export interface OrderPairs {
  event: "getAvailablePairs";
}


/**
 * What kind of order
 */
export type OrderType = "market" | "limit";


/**
 * Message update for a ticker
 */
export interface PairPriceUpdate {
  tradeName?: string;
  ask?: number;
  bid?: number;
}


/**
 * ExchangePair that supports pricing
 */
export type PricedPair = ({
  index: number;
  name: string;
  tradename: string;
  decimals: number;
  baseName: string;
  quoteName: string;
  makerFee: number;
  takerFee: number;
  volume: number;
  ordermin: number;
  ask?: number;
  bid?: number;
} & {
  quoteIndex: number;
  baseIndex: number;
}) & {
  lastAskPrice?: number;
  lastBidPrice?: number;
  bid?: number;
  ask?: number;
};


export type SocketEvent = "open" | "message";


