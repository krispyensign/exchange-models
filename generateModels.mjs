// eslint-disable-next-line node/no-unpublished-import
import pkg from "json-schema-to-typescript";
let { compileFromFile } = pkg;
import yaml from "js-yaml";
import {
  readdirSync,
  createWriteStream,
  mkdirSync,
  statSync,
  readFileSync,
  writeFileSync,
} from "fs";

// switch to the models directory
let projectDir = process.cwd()
process.chdir(`${projectDir}`)

// create some helper strings
let modelsRoot = `${projectDir}/`
let generatedRoot = `${projectDir}/generated/`

// get the list of files
let items = readdirSync(".");

// create the generated directory
try {
  mkdirSync(generatedRoot);
} catch (e) {
  if (e.code && e.code === "EEXIST") {
    console.log(`${generatedRoot} already exists. skipping.`);
  } else {
    throw e;
  }
}

// loop through the files
for (let directory of items) {
  // test if its a directory or not
  if (directory === "generated") continue;
  if (statSync(directory).isDirectory()) {
    try {
      mkdirSync(generatedRoot + directory);
    } catch (e) {
      if (e.code && e.code === "EEXIST") {
        console.log(`${generatedRoot + directory} already exists. skipping.`);
      } else {
        throw e;
      }
    }

    // convert files from yaml to json for consumption
    console.log("Converting files - " + directory);
    let filesToConvert = readdirSync(modelsRoot + directory)
      .filter((file) => file.endsWith(".yaml") || file.endsWith(".yml"))
      .map((file) => [
        file,
        file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf(".")),
      ]);

    for (let [ymlFile, file] of filesToConvert) {
      writeFileSync(
        generatedRoot + directory + "/" + file + ".json",
        JSON.stringify(
          yaml.load(
            readFileSync(modelsRoot + directory + "/" + ymlFile, {
              encoding: "utf-8",
            })
          ),
          null,
          2
        )
      );
    }

    // create a new index .d.ts file
    let indexfile = createWriteStream(`${directory}.d.ts`).on(
      "error",
      (err) => {
        throw err;
      }
    );

    console.log("Compiling files - " + directory);
    indexfile.write("// tslint:disable \n\n");
    let filesToCompile = readdirSync(generatedRoot + directory)
      .filter((file) => file.endsWith(".json"))
      .map((file) => {
        console.log(file);
        return file;
      });

    // attempt to compile the json in the directory into a single .d.ts file
    for (let file of filesToCompile) {
      indexfile.write(
        await compileFromFile(generatedRoot + directory + "/" + file, {
          cwd: generatedRoot + directory,
          bannerComment: "",
          declareExternallyReferenced: false,
        }) + "\n\n"
      );
    }

    // close the file
    indexfile.end();
  }
}
